package kamisado.model;

import java.awt.*;

public enum ColorEnum {
	ORANGE("orange", new Color(215, 117, 34)),
	BLUE("blue", new Color(0, 106, 172)),
	PURPLE("purple", new Color(111, 55, 135)),
	PINK("pink", new Color(209, 112, 157)),
	YELLOW("yellow", new Color(227, 195, 1)),
	RED("red", new Color(207, 50, 56)),
	GREEN("green", new Color(0, 145, 87)),
	BROWN("brown", new Color(87, 38, 0)),
	WHITE("white", new Color(255, 255, 255)),
	BLACK("black", new Color(0, 0, 0));
	
	private String name;
	private Color color;
	
	ColorEnum(String name, Color color) {
		this.name = name;
		this.color = color;
	}
	
	public Color getColor() {
		return this.color;
	}
	
	public String getColorName() {
		return this.name;
	}
	
	public String toString() {
		return this.name;
	}
	
}
