package kamisado.controller;

import kamisado.model.ColorEnum;
import kamisado.model.Player;
import kamisado.ui.CellUi;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PlayerController implements ActionListener {
	
	GameController gameController;
	Player player;
	CellUi selectedCell;
	CellUi selectedTower;
	
	
	public PlayerController(GameController game, ColorEnum color) {
		this.gameController = game;
		this.player = new Player(color);
	}
	
	/**
	 * Detecting which button type has been pressed
	 * And moving tower according to it
	 *
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		this.selectedCell = (CellUi) e.getSource();
		if(this.gameController.getCurrentPlayerController().equals(this)) {
			if(this.selectedCell.isTower()) {
				this.selectedTower = this.selectedCell;
				this.gameController.getBoard().hideAllPlayableCell();
				this.gameController.getBoard().showAllPlayableCell(this, this.selectedTower);
			}
			else if(this.selectedCell.isPlayable()
				&& (this.gameController.getWaitingPlayerController().getSelectedCell() == null
				|| (this.selectedTower.getTowerColor().equals(this.gameController.getWaitingPlayerController().getSelectedCell().getCellColor())))) {
				this.selectedTower.moveTower(this.selectedCell, this);
				this.gameController.getBoard().hideAllPlayableCell();
				this.gameController.tick();
			}
		}
	}
	
	public void selectTower(CellUi cell) {
		this.selectedTower = cell;
		this.gameController.getBoard().hideAllPlayableCell();
		this.gameController.getBoard().showAllPlayableCell(this, this.selectedTower);
	}
	
	/**
	 * Retrieves our player informations from the model
	 *
	 * @return
	 */
	public Player getPlayer() {
		return this.player;
	}
	
	/**
	 * Return the tower selected by the current player
	 *
	 * @return
	 */
	public CellUi getSelectedTower() {
		return this.selectedTower;
	}
	
	public CellUi getSelectedCell() {
		return this.selectedCell;
	}
	
	/**
	 * Comparing two players, thanks to their playerId
	 *
	 * @param playerController
	 * @return
	 */
	public boolean equals(PlayerController playerController) {
		return this.getPlayer().getPlayerId() == playerController.getPlayer().getPlayerId();
	}
	
}
