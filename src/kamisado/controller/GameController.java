package kamisado.controller;

import kamisado.App;
import kamisado.model.Board;
import kamisado.model.ColorEnum;
import kamisado.model.Game;
import kamisado.ui.BoardUi;
import kamisado.ui.CellUi;
import kamisado.ui.GameUi;

public class GameController {
	private Board board;
	private BoardUi boardUi;
	private Game game;
	private GameUi gameUi;
	private PlayerController playerWhite;
	private PlayerController playerBlack;
	private ButtonController buttonController;
	
	private PlayerController currentPlayer;
	private PlayerController waitingPlayer;
	
	public GameController() {
		this.game = new Game();
		this.board = new Board(this, this.game.getCellSpread());
		this.buttonController = new ButtonController(this);
		
		this.boardUi = new BoardUi(this.board);
		this.gameUi = new GameUi(this.game, this.buttonController, this.boardUi);
		this.gameUi.showGameUi(true);
		this.buttonController.setGameUi(this.gameUi);
		
		this.playerWhite = new PlayerController(this, ColorEnum.WHITE);
		this.playerBlack = new PlayerController(this, ColorEnum.BLACK);
		
		this.currentPlayer = this.playerWhite;
		this.waitingPlayer = this.playerBlack;
		
		this.initCellsControllers();
		this.board.initTowers(this.playerWhite, 0);
		this.board.initTowers(this.playerBlack, 7);

	}
	
	/**
	 * Event tick
	 */
	protected void tick() {
		// no tick until first move
		this.game.incrementRound();
		this.toggleCurrentPlayerController();
		this.currentPlayer.selectTower(this.getLinkedCellAndTower());
		this.gameUi.updateCurrentPlayer(this.currentPlayer.getPlayer().getColorName());
		this.gameUi.updateCurrentTower(this.currentPlayer.getSelectedTower().getTowerColor().getColorName());
		this.gameUi.updateCurrentRound(this.game.getRound());
		this.hasWinner();
	}
	
	/**
	 * Do we have a winner ?
	 */
	private void hasWinner() {
		PlayerController currentPlayer = playerWhite;
		PlayerController cellPlayer = null;
		for(int y : new int[]{0, 7}) {
			for(int x = 0; x < this.game.getCellSpread(); x++) {
				cellPlayer = this.board.getCellAt(y, x).getPlayerController();
				if(cellPlayer != null && !cellPlayer.equals(currentPlayer)) {
					System.out.println("[*] We have a winner");
					this.gameUi.showWinDialog(this.game.getWinMessage(waitingPlayer.getPlayer().getColorName()),
						this.game.getWinTitle());
					return;
				}
			}
			currentPlayer = playerBlack;
		}
		System.out.println("[*] No winner - keep playing");
	}
	
	/**
	 * Let's restart the game
	 */
	public void newGame() {
		this.gameUi.showGameUi(false);
		this.board = new Board(this, this.game.getCellSpread());
		this.boardUi = new BoardUi(this.board);
		this.gameUi = new GameUi(this.game, this.buttonController, this.boardUi);
		this.gameUi.showGameUi(true);
		
		this.currentPlayer = this.playerWhite;
		this.waitingPlayer = this.playerBlack;
		
		this.initCellsControllers();
		this.board.initTowers(this.playerWhite, 0);
		this.board.initTowers(this.playerBlack, 7);
	}
	
	/**
	 * Init default player controller, on default cells
	 */
	private void initCellsControllers() {
		PlayerController controller = this.playerWhite;
		for(int y : new int[]{0, 7}) {
			for(CellUi cell : this.board.getAllCell()[y]) {
				this.setCellController(cell, controller);
			}
			controller = this.playerBlack;
		}
	}
	
	/**
	 * Toggle both player controllers
	 */
	protected void toggleCurrentPlayerController() {
		if(currentPlayer.getPlayer().getPlayerId() == 1) {
			this.currentPlayer = this.playerBlack;
			this.waitingPlayer = this.playerWhite;
			System.out.println(" - Player WHITE moved");
		}
		else {
			this.currentPlayer = this.playerWhite;
			this.waitingPlayer = this.playerBlack;
			System.out.println(" - Player BLACK moved");
		}
	}
	
	/**
	 * Return the opponent colored tower corrspoding to played cell by previous player
	 * @return
	 */
	protected CellUi getLinkedCellAndTower() {
		CellUi selectedTower;
		for(int y = 0; y < this.game.getCellSpread(); y++) {
			for(int x = 0; x < this.game.getCellSpread(); x++) {
				selectedTower = this.board.getCellAt(y, x);
				if(selectedTower.getTowerColor() != null
					&& this.waitingPlayer.getSelectedCell().getCellColor().equals(selectedTower.getTowerColor())
					&& this.currentPlayer.equals(selectedTower.getPlayerController())) {
					return selectedTower;
				}
			}
		}
		return null;
	}
	
	/**
	 * Set PlayerController over one specific cell
	 *
	 * @param cell
	 * @param controller
	 */
	protected void setCellController(CellUi cell, PlayerController controller) {
		cell.attachTower(controller);
	}
	
	/**
	 * Reset PlayerController over one specific cell
	 *
	 * @param cell
	 */
	protected void resetCellController(CellUi cell) {
		cell.detachTower();
	}
	
	/**
	 * Return Board reference,
	 * needed for PlayerController actions
	 *
	 * @return
	 */
	protected Board getBoard() {
		return this.board;
	}
	
	/**
	 * Return the current player controller
	 */
	public PlayerController getCurrentPlayerController() {
		return this.currentPlayer;
	}
	
	/**
	 * Return the waiting player controller
	 */
	public PlayerController getWaitingPlayerController() {
		return this.waitingPlayer;
	}
	
	/**
	 * Get game info object
	 */
	public Game getGame() {
		return this.game;
	}
	
}
