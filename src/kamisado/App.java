/*
 * Romain GONCALVES
 * S2B2
 *
 * Projet :: Kamisado
 */

package kamisado;

import kamisado.controller.GameController;

public class App {
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				GameController gameController = new GameController();
			}
		});
	}
	
}
