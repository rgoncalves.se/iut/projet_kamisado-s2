package kamisado.model;

public class Game {
	
	private String title;
	private int windowSize;
	private int cellSpread;
	private int round;
	private boolean resizeable;
	private String winMessage;
	private String winTitle;
	
	/**
	 * Initialisation of model's values
	 */
	public Game() {
		this.title = "Kamisado";
		this.windowSize = 640;
		this.cellSpread = 8;
		this.resizeable = false;
		this.winMessage = "We have a winner !";
		this.winTitle = "Game over !";
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getWindowSize() {
		return this.windowSize;
	}
	
	public String getWinMessage(String playerInfo) {
		return this.winMessage + "\nPlayer " + playerInfo + " won !!";
	}
	
	public String getWinTitle() {
		return this.winTitle;
	}
	
	public int getCellSpread() {
		return this.cellSpread;
	}
	
	public boolean isResizeable() {
		return this.resizeable;
	}
	
	public void incrementRound() {
		this.round++;
	}
	
	public int getRound() {
		return this.round;
	}
	
}
