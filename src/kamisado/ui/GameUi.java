package kamisado.ui;

import kamisado.controller.ButtonController;
import kamisado.model.Game;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class GameUi extends JFrame {
	
	private Game game;
	private BoardUi boardUi;
	private MenuUi menuUi;
	
	private JLabel playerLabel;
	private JLabel towerLabel;
	private JLabel roundLabel;
	
	/**
	 * This is our GameUi,
	 * containing toolBar, boardUi, and statusBar
	 * @param game
	 * @param buttonController
	 * @param boardUi
	 */
	public GameUi(Game game, ButtonController buttonController, BoardUi boardUi) {
		this.game = game;
		this.boardUi = boardUi;
		this.menuUi = new MenuUi(buttonController);
		
		this.setTitle(this.game.getTitle());
		this.setSize(this.game.getWindowSize(), this.game.getWindowSize());
		this.setResizable(this.game.isResizeable());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setJMenuBar(this.menuUi);
		this.add(this.boardUi);
		this.createBottomStatusBar();
		this.add(this.createBottomStatusBar(), BorderLayout.SOUTH);
	}
	
	/**
	 * Show or not, the gameUi depending on a boolean
	 * @param state
	 */
	public void showGameUi(boolean state) {
		this.setVisible(state);
	}
	
	/**
	 * Shows a popup dialog with specific title and message
	 * @param winMessage
	 * @param winTitle
	 */
	public void showWinDialog(String winMessage, String winTitle) {
		JOptionPane winPane = new JOptionPane();
		winPane.showMessageDialog(this, winMessage, winTitle, JOptionPane.OK_OPTION);
	}
	
	/**
	 * Create the bottom status bar,
	 * showing the current player, current tower, and round number
	 */
	private JPanel createBottomStatusBar() {
		// Bottom panel
		JPanel statusPanel = new JPanel();
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusPanel.setPreferredSize(new Dimension(this.getWidth(), 16));
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		
		// Player
		JLabel playerLabelStatic = new JLabel("player : ");
		this.playerLabel = new JLabel("white");
		
		// Tower
		JLabel towerLabelStatic = new JLabel("tower : ");
		this.towerLabel = new JLabel("");
		
		// Round
		JLabel roundLabelStatic = new JLabel("round : ");
		this.roundLabel = new JLabel("0");
		
		
		// Feeding the bottom panel with objects
		playerLabelStatic.setHorizontalAlignment(SwingConstants.LEFT);
		this.playerLabel.setHorizontalAlignment(SwingConstants.LEFT);
		towerLabelStatic.setHorizontalAlignment(SwingConstants.LEFT);
		this.towerLabel.setHorizontalAlignment(SwingConstants.LEFT);
		roundLabelStatic.setHorizontalAlignment(SwingConstants.RIGHT);
		this.roundLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		
		statusPanel.add(playerLabelStatic);
		statusPanel.add(this.playerLabel);
		statusPanel.add(new JLabel("        |        "));
		statusPanel.add(towerLabelStatic);
		statusPanel.add(this.towerLabel);
		statusPanel.add(new JLabel("        |        "));
		statusPanel.add(roundLabelStatic);
		statusPanel.add(this.roundLabel);
		
		return statusPanel;
	}
	
	public void updateCurrentPlayer(String playerColor) {
		this.playerLabel.setText(playerColor);
	}
	
	public void updateCurrentTower(String towerColor) {
		this.towerLabel.setText(towerColor);
	}
	
	public void updateCurrentRound(int round) {
		this.roundLabel.setText(Integer.toString(round));
	}
	
	public MenuUi getMenuUi() {
		return this.menuUi;
	}

}
