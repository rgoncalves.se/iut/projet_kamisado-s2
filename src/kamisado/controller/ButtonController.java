package kamisado.controller;

import kamisado.ui.GameUi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonController implements ActionListener {
	
	private GameController gameController;
	private GameUi gameUi;
	
	public ButtonController(GameController gameController) {
		this.gameController = gameController;
	}
	
	public void setGameUi(GameUi gameUi) {
		this.gameUi = gameUi;
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.gameUi.getMenuUi().getItemNewGame()) {
			this.gameController.newGame();
		}
		if(e.getSource() == this.gameUi.getMenuUi().getItemExit()) {
			System.exit(0);
		}
	}
	
}
