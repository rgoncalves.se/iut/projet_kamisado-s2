package kamisado.model;

import kamisado.controller.GameController;
import kamisado.controller.PlayerController;
import kamisado.ui.CellUi;

public class Board {
	
	private int cellSpread;
	private CellUi[][] cells;
	private GameController gameController;
	
	private ColorEnum[] cellColor = {
		ColorEnum.ORANGE,
		ColorEnum.BLUE,
		ColorEnum.PURPLE,
		ColorEnum.PINK,
		ColorEnum.YELLOW,
		ColorEnum.RED,
		ColorEnum.GREEN,
		ColorEnum.BROWN
	};
	private int[][] cellColorGrid = {
		{0, 1, 2, 3, 4, 5, 6, 7},
		{5, 0, 3, 6, 1, 4, 7, 2},
		{6, 3, 0, 5, 2, 7, 4, 1},
		{3, 2, 1, 0, 7, 6, 5, 4},
		{4, 5, 6, 7, 0, 1, 2, 3},
		{1, 4, 7, 2, 5, 0, 3, 6},
		{2, 7, 4, 1, 6, 3, 0, 5},
		{7, 6, 5, 4, 3, 2, 1, 0}
	};
	
	private int oneCount = 0;
	private int twoCount = 0;
	private int threeCount = 0;
	private int fourCount = 0;
	
	private boolean downRightStop = false;
	private boolean downLeftStop = false;
	
	/**
	 * Board model, controlled by playerController and gameController
	 * Initialisations are done by the gameController
	 * Movements are played by one of the playerController
	 *
	 * @param gameController
	 * @param cellSpread
	 */
	public Board(GameController gameController, int cellSpread) {
		this.gameController = gameController;
		this.cellSpread = cellSpread;
		
		this.cells = new CellUi[this.cellSpread][this.cellSpread];
		this.initBoardCells();
	}
	
	/**
	 * Initialisation of all of the cells of the board
	 * Looking for pattern and colors determind previously
	 */
	private void initBoardCells() {
		int colorIndex;
		for(int y = 0; y < cellSpread; y++) {
			for(int x = 0; x < cellSpread; x++) {
				this.cells[y][x] = new CellUi(this.getColorAt(y, x), y, x);
			}
		}
	}
	
	/**
	 * Initialisation of towers colors and positions
	 * Color variations depends on the playerController
	 */
	public CellUi[] initTowers(PlayerController playerController, int index) {
		CellUi[] tab = new CellUi[this.cellSpread];
		for(int x = 0; x < cellSpread; x++) {
			ColorEnum selectedColor = this.getColorAt(index, x);
			this.cells[index][x].initTower(selectedColor, playerController);
		}
		return tab;
	}
	
	/**
	 * Show all playable cells, from one selected tower
	 * This method is broke down in multiple other method, for each player and direction
	 *
	 * @param playerController
	 * @param originCell
	 */
	public void showAllPlayableCell(PlayerController playerController, CellUi originCell) {
		for(int x = 0; x < this.cellSpread; x++) {
			if(playerController.getPlayer().getPlayerId() == 1) {
				for(int y = originCell.getYindex(); y < this.getVerticalLimit(originCell); y++) {
					this.showVerticalPlayableCell(y, x, originCell);
				}
			}
			else {
				for(int y = originCell.getYindex(); y >= this.getVerticalLimit(originCell); y--) {
					this.showVerticalPlayableCell(y, x, originCell);
				}
			}
		}
		if(playerController.getPlayer().getPlayerId() == 1){
			this.showDiagonaleWhiteLeft(originCell);
			this.showDiagonaleWhiteRight(originCell);
		}
		else {
			this.showDiagonaleBlackLeft(originCell);
			this.showDiagonaleBlackRight(originCell);
		}
	}
	
	/**
	 * Grab all the cell marked playable and hide them
	 */
	public void hideAllPlayableCell() {
		for(int y = 0; y < this.cellSpread; y++) {
			for(int x = 0; x < this.cellSpread; x++) {
				if(!this.cells[y][x].isTower() && this.cells[y][x].isPlayable())
					this.cells[y][x].detachPlayableCell();
			}
		}
	}
	
	/**
	 * Show all playable cell vertically
	 *
	 * @param y
	 * @param x
	 * @param originCell
	 */
	private void showVerticalPlayableCell(int y, int x, CellUi originCell) {
		if(!this.cells[y][x].isTower()) {
			if(this.cells[y][x].getXindex() == originCell.getXindex()) {
				this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
			}
		}
	}
	
	/**
	 * Show left diagonal for White player
	 * @param originCell
	 */
	private void showDiagonaleWhiteLeft(CellUi originCell) {
		int count = 0;
		for(int y = originCell.getYindex() + 1; y < this.getCellSpread(); y++) {
			for(int x = originCell.getXindex() - 1; x >= 0; x--) {
				for(int i = 0; i < this.getCellSpread(); i++) {
					if(originCell.getYindex() == y - i && originCell.getXindex() == x + i) {
						if(!this.cells[y][x].isTower()) {
							if(this.cells[y-1][x+1].isTower()) {
								count++;
							}
							if(count >= 2)
								return;
							this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Show right diagonal for White player
	 * @param originCell
	 */
	private void showDiagonaleWhiteRight(CellUi originCell) {
		int count = 0;
		for(int y = originCell.getYindex() + 1; y < this.getCellSpread(); y++) {
			for(int x = originCell.getXindex() + 1; x < this.getCellSpread(); x++) {
				for(int i = 0; i < this.getCellSpread(); i++) {
					if(originCell.getYindex() == y - i && originCell.getXindex() == x - i) {
						if(!this.cells[y][x].isTower()) {
							if(this.cells[y-1][x-1].isTower()) {
								count++;
							}
							if(count >= 2)
								return;
							this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Show left diagonal for Black player
	 * @param originCell
	 */
	private void showDiagonaleBlackLeft(CellUi originCell) {
		int count = 0;
		for(int y = originCell.getYindex() - 1; y >= 0; y--) {
			for(int x = originCell.getXindex() - 1; x >= 0; x--) {
				for(int i = 0; i < this.getCellSpread(); i++) {
					if(originCell.getYindex() == y + i && originCell.getXindex() == x + i) {
						if(!this.cells[y][x].isTower()) {
							if(this.cells[y+1][x+1].isTower()) {
								count++;
							}
							if(count >= 2)
								return;
							this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
						}
					}
				}
			}
		}
	}
	
	/**
	 * Show right diagonal for Black player
	 * @param originCell
	 */
	private void showDiagonaleBlackRight(CellUi originCell) {
		int count = 0;
		for(int y = originCell.getYindex() - 1; y >= 0; y--) {
			for(int x = originCell.getXindex() + 1; x < this.getCellSpread(); x++) {
				for(int i = 0; i < this.getCellSpread(); i++) {
					if(originCell.getYindex() == y + i && originCell.getXindex() == x - i) {
						if(!this.cells[y][x].isTower()) {
							if(this.cells[y+1][x-1].isTower()) {
								count++;
							}
							if(count >= 2)
								return;
							this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
						}
					}
				}
			}
		}
	}


	/**
	 * Show all playable cells on diagonal, from bottom to top
	 * This is the one used for black playerController
	 *
	 * @param y
	 * @param x
	 * @param originCell
	 */
	private void showDiagonalPlayableCellUp(int y, int x, CellUi originCell) {
		for(int i = 0; i < this.cellSpread; i++) {
			if(originCell.getYindex() == y + i
				&& originCell.getXindex() == x + i) {
				if(this.cells[y][x].isTower()) {
					this.threeCount++;
				}
				if(!this.cells[y][x].isTower() && this.threeCount < 2) {
					this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
				}
			}
			else if(originCell.getYindex() == y + i
				&& originCell.getXindex() == x - i) {
				if(this.cells[y][x].isTower()) {
					this.fourCount++;
				}
				if(!this.cells[y][x].isTower() && this.fourCount < 1) {
					this.cells[y][x].attachPlayableCell(this.gameController.getCurrentPlayerController());
				}
			}
		}
	}
	
	/**
	 * Given a cell (mostly a tower), return the max height it can move vertically
	 * It depends on the active playerController in the gameController
	 *
	 * @param cell
	 * @return
	 */
	private int getVerticalLimit(CellUi cell) {
		if(cell.getPlayerController().getPlayer().getPlayerId() == 1) {
			int y = cell.getYindex() + 1;
			while(y != this.cellSpread && !this.cells[y][cell.getXindex()].isTower()) {
				y++;
			}
			return y;
		}
		int y = cell.getYindex() - 1;
		while(y != 0 && !this.cells[y][cell.getXindex()].isTower()) {
			y--;
		}
		return y;
	}
	
	/**
	 * Return the color of a specific cell
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public ColorEnum getColorAt(int y, int x) {
		return this.cellColor[this.cellColorGrid[y][x]];
	}
	
	/**
	 * Return the number of cells per side
	 * (currently a constant, but might increase if we do bigger board one day)
	 *
	 * @return
	 */
	public int getCellSpread() {
		return this.cellSpread;
	}
	
	/**
	 * Return the cell object at a specific index
	 *
	 * @param y
	 * @param x
	 * @return
	 */
	public CellUi getCellAt(int y, int x) {
		return this.cells[y][x];
	}
	
	/**
	 * Return all the cell in an array, used by outside methods
	 *
	 * @return
	 */
	public CellUi[][] getAllCell() {
		return this.cells;
	}
}
