package kamisado.ui;

import kamisado.model.Board;
import javax.swing.*;
import java.awt.*;

public class BoardUi extends JPanel {
	
	private Board board;
	
	public BoardUi(Board board) {
		this.board = board;
		this.setLayout(new GridLayout(this.board.getCellSpread(), this.board.getCellSpread()));
		this.fillGridContent();
	}
	
	private void fillGridContent() {
		for(int i = 0; i < this.board.getCellSpread(); i++) {
			for(int j = 0; j < this.board.getCellSpread(); j++) {
				this.add(this.board.getCellAt(i, j));
			}
		}
	}
}
