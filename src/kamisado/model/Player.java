package kamisado.model;

import java.awt.*;

public class Player {
	
	static int idCount;
	private int playerId;
	private Color playerColor;
	private String playerColorName;
	
	public Player(ColorEnum color) {
		idCount += 1;
		this.playerId = idCount;
		this.playerColor = color.getColor();
		this.playerColorName = color.getColorName();
	}
	
	public int getPlayerId() {
		return this.playerId;
	}
	
	public Color getColor() {
		return this.playerColor;
	}
	
	public String getColorName() {
		return this.playerColorName;
	}
	
}
