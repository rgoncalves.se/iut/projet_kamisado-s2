package kamisado.ui;

import kamisado.controller.ButtonController;

import javax.swing.*;

public class MenuUi extends JMenuBar {
	
	private JMenuItem itemNewGame;
	private JMenuItem itemExit;
	
	public MenuUi(ButtonController buttonController) {
		JMenu menu = new JMenu("Options");
		itemNewGame = new JMenuItem("New Game");
		itemExit = new JMenuItem("Exit");
		
		itemNewGame.addActionListener(buttonController);
		itemExit.addActionListener(buttonController);
		
		menu.add(itemNewGame);
		menu.addSeparator();
		menu.add(itemExit);
		
		this.add(menu);
		this.setVisible(true);
	}
	
	public JMenuItem getItemNewGame() {
		return this.itemNewGame;
	}
	
	public JMenuItem getItemExit() {
		return this.itemExit;
	}

	
}
