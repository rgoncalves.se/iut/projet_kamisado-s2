package kamisado.ui;

import kamisado.controller.PlayerController;
import kamisado.model.ColorEnum;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class CellUi extends JButton {
	
	private ImageIcon icon;
	private boolean towerState;
	private ColorEnum towerColor;
	private ColorEnum cellColor;
	private boolean playableState;
	private PlayerController playerController;
	private int x;
	private int y;
	
	/**
	 * Let's construct our cell
	 */
	public CellUi(ColorEnum cellColor, int y, int x) {
		this.icon = null;
		this.towerState = false;
		this.towerColor = null;
		this.cellColor = cellColor;
		this.playableState = true;
		this.y = y;
		this.x = x;
		
		this.setBackground(this.cellColor.getColor());
		this.setOpaque(true);
	}
	
	/**
	 * Initialisation of tower
	 */
	public void initTower(ColorEnum color, PlayerController playerController) {
		this.playerController = playerController;
		this.towerColor = color;
		this.setIconCustom(color.getColorName() + "_" + this.playerController.getPlayer().getColorName());
		this.towerState = true;
	}
	
	/**
	 * Move tower to a new cell
	 */
	public void moveTower(CellUi newCell, PlayerController playerController) {
		newCell.detachPlayableCell();
		newCell.attachTower(playerController);
		newCell.setIcon(this.getIcon());
		newCell.setTowerColor(this.towerColor);
		this.setIcon(null);
		this.towerColor = null;
		this.detachTower();
	}
	
	/**
	 * Set individual icon
	 * @param iconName
	 */
	private void setIconCustom(String iconName) {
		final String path = "resources/images/" + iconName + ".png";
		try {
			this.icon = new ImageIcon((ImageIO.read(getClass().getResource(path))));
			this.setIcon(icon);
		} catch(Exception e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Attach tower to cell
	 * @param playerController
	 */
	public void attachTower(PlayerController playerController) {
		this.playerController = playerController;
		this.addActionListener(playerController);
		this.towerState = true;
	}
	
	/**
	 * Detach tower to cell
	 */
	public void detachTower() {
		this.playerController = null;
		this.removeActionListener(this.getActionListeners()[0]);
		this.towerState = false;
	}
	
	/**
	 * Attach playable cell
	 */
	public void attachPlayableCell(PlayerController playerController) {
		if(playerController.getPlayer().getColorName() == "white")
			this.setIconCustom("playable_white");
		else
			this.setIconCustom("playable_black");
		this.addActionListener(playerController);
		this.playableState = true;
	}
	
	/**
	 * Attach playable cell
	 */
	public void detachPlayableCell() {
		if(this.getActionListeners().length == 1) {
			this.removeActionListener(this.getActionListeners()[0]);
		}
		this.setIcon(null);
		this.playableState = false;
	}
	
	/**
	 * Change tower color
	 */
	public void setTowerColor(ColorEnum towerColor) {
		this.towerColor = towerColor;
	}
	
	/**
	 * Return tower color
	 */
	public ColorEnum getTowerColor() {
		return this.towerColor;
	}
	
	/**
	 * Return cell color
	 */
	public ColorEnum getCellColor() {
		return this.cellColor;
	}
	
	/**
	 * Return the current player controller
	 * @return
	 */
	public PlayerController getPlayerController() {
		return this.playerController;
	}
	
	/**
	 * Return true if the current object is a tower
	 * @return
	 */
	public boolean isTower() {
		return this.towerState;
	}
	
	/**
	 * Return true if the current object is playable
	 */
	public boolean isPlayable() {
		return this.playableState;
	}
	
	/**
	 * Return cell Y location in the grid
	 * @return
	 */
	public int getYindex() {
		return this.y;
	}
	
	/**
	 * Return cell X location in the grid
	 * @return
	 */
	public int getXindex() {
		return this.x;
	}

}
